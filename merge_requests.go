package main

import (
	"fmt"
	"os"
)

func PrepareMergeRequest(
	projectID,
	assigneeID,
	sourceBranch,
	targetBranch *string,
	deleteSourceBranch *bool,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		BaseAddr:    os.Getenv("CI_API_V4_URL"),
		Path:        "merge_requests",
		ProjectID:   *projectID,
		ContentType: "application/json",
	}

	data := &MapInterface{
		"title": fmt.Sprintf(
			"sync %s@%s",
			*sourceBranch,
			*targetBranch,
		),
		"description":                  "automated sync",
		"assignee_id":                  *assigneeID,
		"id":                           *projectID,
		"source_branch":                *sourceBranch,
		"target_branch":                *targetBranch,
		"remove_source_branch":         *deleteSourceBranch,
		"merge_when_pipeline_succeeds": true,
	}
	return request, data
}

func PrepareMergeAccept(
	internalID, projectID, assigneeID, sourceBranch, targetBranch *string,
	deleteSourceBranch *bool,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		Path: fmt.Sprintf(
			"merge_requests/%s/merge",
			*internalID,
		),
		BaseAddr:    os.Getenv("CI_API_V4_URL"),
		ProjectID:   *projectID,
		ContentType: "application/json",
	}
	data := &MapInterface{
		"merge_commit_message": fmt.Sprintf(
			"sync %s@%s",
			*sourceBranch,
			*targetBranch,
		),
		"assignee_id":                  *assigneeID,
		"id":                           *projectID,
		"should_remove_source_branch":  *deleteSourceBranch,
		"merge_when_pipeline_succeeds": true,
	}
	return request, data
}

func PrepareGetMergeRequest(
	internalID, projectID *string,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		Path: fmt.Sprintf(
			"merge_requests/%s",
			*internalID,
		),
		BaseAddr:    os.Getenv("CI_API_V4_URL"),
		ProjectID:   *projectID,
		ContentType: "application/json",
	}
	data := &MapInterface{
		"id":                             *projectID,
		"merge_request_iid":              *internalID,
		"include_diverged_commits_count": true,
		"include_rebase_in_progress":     true,
	}
	return request, data
}

func PreparePutMergeRequest(
	internalID, projectID *string,
	newData *MapInterface,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		Path: fmt.Sprintf(
			"merge_requests/%s",
			*internalID,
		),
		BaseAddr:    os.Getenv("CI_API_V4_URL"),
		ProjectID:   *projectID,
		ContentType: "application/json",
	}
	fixData := &MapInterface{
		"id":                *projectID,
		"merge_request_iid": *internalID,
	}

	merged := make(MapInterface)

	for k, v := range *newData {
		merged[k] = v
	}
	for k, v := range *fixData {
		merged[k] = v
	}
	return request, &merged
}

func (c *Cli) MergeRequest(
	projectID,
	assigneeID,
	sourceBranch,
	targetBranch *string,
	silentPtr,
	deleteSourceBranch *bool,
) int {
	merge, mergeData := PrepareMergeRequest(
		projectID,
		assigneeID,
		sourceBranch,
		targetBranch,
		deleteSourceBranch,
	)
	mergeResponse, err := c.client.Do(
		merge.prepareRequest("POST", mergeData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	body := *handleResponse(silentPtr, mergeResponse)
	mergeRequestBody := body.(map[string]interface{})
	internalID := int(mergeRequestBody["iid"].(float64))

	return internalID
}

func (c *Cli) GetMergeRequest(
	projectID, internalID *string,
	silentPtr *bool,
) MapInterface {
	merge, mergeData := PrepareGetMergeRequest(
		projectID,
		internalID,
	)
	mergeResponse, err := c.client.Do(
		merge.prepareRequest("GET", mergeData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	body := *handleResponse(silentPtr, mergeResponse)
	mergeRequestBody := body.(map[string]interface{})

	return mergeRequestBody
}

func (c *Cli) PutMergeRequest(
	projectID, internalID *string,
	data *MapInterface,
	silentPtr *bool,
) map[string]interface{} {

	merge, mergeData := PreparePutMergeRequest(
		projectID,
		internalID,
		data,
	)
	mergeResponse, err := c.client.Do(
		merge.prepareRequest("PUT", mergeData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	body := *handleResponse(silentPtr, mergeResponse)
	mergeRequestBody := body.(map[string]interface{})

	return mergeRequestBody
}

func (c *Cli) MergeAccept(
	internalID, projectID, assigneeID, sourceBranch, targetBranch *string,
	deleteSourceBranch, silentPtr *bool,
) {
	accept, _ := PrepareMergeAccept(
		internalID,
		projectID,
		assigneeID,
		sourceBranch,
		targetBranch,
		deleteSourceBranch)

	acceptResponse, err := c.client.Do(
		accept.prepareRequest("PUT", nil, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	handleResponse(silentPtr, acceptResponse)
}

func (c *Cli) UpdateMergeRequest(internalID, projectID *string, data MapInterface) {}
