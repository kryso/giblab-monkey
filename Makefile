GOOS=linux
GOARCH=amd64

giblab-monkey-$(GOOS)-$(GOARCH): *.go
	go test -coverprofile .testCoverage.txt -v 
	go build \
		 -o ./giblab-monkey-$(GOOS)-$(GOARCH) \
		 -ldflags='-s -w' \
		 -v .

