package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"gopkg.in/yaml.v2"
)

// MapInterface is just a name to a struct with string keys and any values
type MapInterface map[string]interface{}

// Endpoint interface represents a valid gitlab api endpoint that can create a
// proper request.
type Endpoint interface {
	prepareRequest()
}

// APIRequest is a struct that satisfies the Endpoint interface. It implements
// gitlab related api endpoints
type APIRequest struct {
	BaseAddr    string
	Path        string
	ProjectID   string
	ContentType string
}

// construct a url
func (r *APIRequest) constructURL() string {
	return fmt.Sprintf(
		"%s/projects/%s/%s",
		r.BaseAddr,
		r.ProjectID,
		r.Path,
	)
}

// prepareRequest creates a request body, that is sent off later.
func (r *APIRequest) prepareRequest(
	method string,
	data *MapInterface,
	urlArgs *map[string]string,
	silent *bool,
) *http.Request {

	jsonBytes, err := json.Marshal(&data)
	if err != nil {
		panic(err)
	}
	url := r.constructURL()
	request, err := http.NewRequest(
		method, url, bytes.NewBuffer(jsonBytes))

	if err != nil {
		panic(err)
	}
	request.Header.Set(
		"Content-Type", r.ContentType,
	)
	request.Header.Set(
		"PRIVATE-TOKEN",
		os.Getenv("PRIVATE_MAINTAINER_TOKEN"),
	)
	request.ContentLength = int64(len(jsonBytes))
	if urlArgs != nil {
		q := request.URL.Query()
		for k, v := range *urlArgs {
			q.Add(k, v)
		}
		request.URL.RawQuery = q.Encode()
	}
	return request
}

// readBody reads the reponse body.
func readBody(r *http.Response) []byte {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	return body
}

// handleResponse takes the reponse of gitlab and handles it.
func handleResponse(
	silent *bool,
	r *http.Response) *interface{} {

	var responseBody *interface{}
	var message interface{}

	body := readBody(r)
	if len(body) > 0 {
		err := json.Unmarshal(body, &responseBody)
		if err != nil {
			panic(err)
		}
	}
	switch outputFormat {
	case "json":
		message, _ = json.Marshal(&responseBody)
	case "yaml":
		message, _ = yaml.Marshal(&responseBody)
	}

	if 200 <= r.StatusCode && r.StatusCode <= 299 {
		fmt.Fprintf(
			os.Stderr,
			"+++ OK: %d\n",
			r.StatusCode,
		)
		if !*silent {
			fmt.Fprintf(
				os.Stdout,
				"---\n%s\n",
				message,
			)
		}
	} else {
		fmt.Fprintf(
			os.Stderr,
			"!!! Status: %d\n---\n%s",
			r.StatusCode,
			message,
		)
		os.Exit(1)
	}
	return responseBody
}
