package main

import (
	"os"
)

func PrepareNewTag(
	projectID,
	tagName,
	parentRef,
	releaseMessage,
	releaseDescription *string,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		BaseAddr:    os.Getenv("CI_API_V4_URL"),
		Path:        "repository/tags",
		ProjectID:   *projectID,
		ContentType: "application/json",
	}

	data := &MapInterface{
		"id":                  *projectID,
		"tag_name":            *tagName,
		"ref":                 *parentRef,
		"message":             *releaseMessage,
		"release_description": *releaseDescription,
	}
	return request, data
}

func (c *Cli) NewTag(
	projectID,
	tagName,
	parentRef,
	releaseMessage,
	releaseDescription *string,
	silentPtr *bool,
) int {
	tag, tagData := PrepareNewTag(
		projectID,
		tagName,
		parentRef,
		releaseMessage,
		releaseDescription,
	)
	tagResponse, err := c.client.Do(
		tag.prepareRequest("POST", tagData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	handleResponse(silentPtr, tagResponse)
	return 0
}

func PrepareGetTags(
	projectID,
	orderBy,
	sort,
	search *string,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		BaseAddr:    os.Getenv("CI_API_V4_URL"),
		Path:        "repository/tags",
		ProjectID:   *projectID,
		ContentType: "application/json",
	}

	data := &MapInterface{
		"id":       *projectID,
		"order_by": *orderBy,
		"sort":     *sort,
		"search":   *search,
	}
	return request, data
}

func (c *Cli) GetTags(
	projectID,
	orderBy,
	sort,
	search *string,
	silentPtr *bool,
) int {
	tags, tagsData := PrepareGetTags(
		projectID,
		orderBy,
		sort,
		search,
	)
	urlArgs := &map[string]string{
		"order_by": *orderBy,
		"search":   *search,
		"sort":     *sort,
	}

	tagsResponse, err := c.client.Do(
		tags.prepareRequest("GET", tagsData, urlArgs, silentPtr))
	if err != nil {
		panic(err)
	}
	handleResponse(silentPtr, tagsResponse)
	return 0
}
