# giblab-monkey

> a ginger inspired gitlab api automation utility

## installation

### upstream

```
go get gitlab.com/kryso/giblab-monkey
go install gitlab.com/kryso/giblab-monkey
```

### as threeyd duderina or dude

#### v0.2.1
[linux](https://gitlab.3yourmind.com/api/v4/projects/22/jobs/artifacts/v0.2.1/download?job=build-linux-amd64)
[darwin](https://gitlab.3yourmind.com/api/v4/projects/28/jobs/artifacts/v0.2.1/download?job=build-darwin-amd64)
[windows](https://gitlab.3yourmind.com/api/v4/projects/28/jobs/artifacts/v0.2.1/download?job=build-windows-amd64)

>Then unzip and execute!

Do not forget to put it into `$PATH` and make it executable:
```
cp giblab-monkey ~/.local/bin
chmod +x ~/.local/bin/giblab-monkey 
```

## development

0. clone the repo
1. `go get` in the repo directory
2. export `$GOOS` and `$GOARCH` to your target
3. `make`

## usage

The monkey requests specific gitlab api endpoints. Among them are:

- repository_files
- tag creation
- merge_requests/accepting them
- pipeline trigger

### authentication

Authentication is done via a private token, that can be created [here](https://gitlab.3yourmind.com/profile/personal_access_tokens)
> Be sure to atleast set the `api` scope on that token or giblab-monkey will not work.

Gitlab will respond with a 404, when you access a private project where your
token has no scope. The same happens, when the gitlab instance is set to
mandatory 2fa and your token does not allow access to the project.


### base config
To configure the monkey, export these shell variables by any means you have:
```
export CI_API_V4_URL=https://gitlab.3yourmind.com/api/v4
export PRIVATE_MAINTAINER_TOKEN=<the token you created>
```

> Other arguments to any `giblab-monkey` sub command use gitlabs predefined [ci variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) where appriopiate

Other shell variables that are used currently (v0.2.1):
- `PRIVATE_MAINTAINER_TOKEN`
- `TARGET_BRANCH`
- `TARGET_REF`
- `GITLAB_USER_ID`
- `CI_API_V4_URL`
- `CI_PROJECT_ID`
- `CI_COMMIT_REF_NAME`

> These are there to either follow pipeline defaaults or circumvent them. 
