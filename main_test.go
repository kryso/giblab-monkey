package main

import (
	"testing"
)

func TestSemverParseWithV(t *testing.T) {
	ver0, err := Parse("v1.1.1")
	if err != nil {
		panic(err)
	}

	if ver0.Major != 1 {
		t.Errorf("Expected Major code %d. Got %d\n", 0, ver0.Major)
	}
	if ver0.Minor != 1 {
		t.Errorf("Expected Major code %d. Got %d\n", 0, ver0.Minor)
	}
	if ver0.Patch != 1 {
		t.Errorf("Expected Major code %d. Got %d\n", 0, ver0.Patch)
	}
}

func TestSemverParse(t *testing.T) {
	ver0, err := Parse("1.1.1")
	if err != nil {
		panic(err)
	}

	if ver0.Major != 1 {
		t.Errorf("Expected Major code %d. Got %d\n", 0, ver0.Major)
	}
	if ver0.Minor != 1 {
		t.Errorf("Expected Major code %d. Got %d\n", 0, ver0.Minor)
	}
	if ver0.Patch != 1 {
		t.Errorf("Expected Major code %d. Got %d\n", 0, ver0.Patch)
	}
}

func TestSemverCompare(t *testing.T) {
	ver0, err := Parse("v1.1.1")
	if err != nil {
		panic(err)
	}
	ver1, err := Parse("v1.1.2")
	if err != nil {
		panic(err)
	}
	ver2, err := Parse("v1.2.0")
	if err != nil {
		panic(err)
	}

	ver01 := ver0.Compare(ver1)
	ver02 := ver0.Compare(ver2)
	ver20 := ver2.Compare(ver0)

	if ver01 != -1 {
		t.Errorf("Expected version code comparison %d. Got %d\n", -1, ver01)
	}
	if ver02 != -1 {
		t.Errorf("Expected version code comparison %d. Got %d\n", -1, ver02)
	}
	if ver20 != 1 {
		t.Errorf("Expected version code comparison %d. Got %d\n", 1, ver20)
	}
}

func TestSemverNewMajor(t *testing.T) {
	ver, err := Parse("v1.1.1")
	if err != nil {
		panic(err)
	}
	newVer := ver.NewMajor()
	if newVer.Major != 2 {
		t.Errorf("Expected version code comparison %d. Got %d\n", 2, newVer.Major)
	}

}
