package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type Cli struct {
	client *http.Client
	//register *map[string]interface{}
}

var (
	outputFormat string = "json"
	silent       bool   = true
)

func (c *Cli) runNewFile() {
	newFileCmd := flag.NewFlagSet("newfile", flag.ExitOnError)
	silentPtr := newFileCmd.Bool(
		"silent", false, "shut up monkey.")
	targetBranch := newFileCmd.String(
		"target",
		os.Getenv("TARGET_BRANCH"),
		"the new file will be created on this git ref")
	projectID := newFileCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	filePath := newFileCmd.String(
		"path",
		"version",
		"the path of the file in the repo")
	localFilePath := newFileCmd.String(
		"file",
		"/dev/stdin",
		"the local path of the file")
	commitMessage := newFileCmd.String(
		"msg",
		"giblab-monkey-commit",
		"the path of the file in the repo")

	newFileCmd.Parse(os.Args[2:])
	c.NewFile(
		projectID,
		targetBranch,
		commitMessage,
		filePath,
		localFilePath,
		silentPtr,
	)
}

func (c *Cli) runUpdateFile() {
	newFileCmd := flag.NewFlagSet("updatefile", flag.ExitOnError)
	silentPtr := newFileCmd.Bool(
		"silent", false, "shut up monkey.")
	targetBranch := newFileCmd.String(
		"target",
		os.Getenv("TARGET_BRANCH"),
		"the new file will be created on this git ref")
	projectID := newFileCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	filePath := newFileCmd.String(
		"path",
		"version",
		"the path of the file in the repo")
	localFilePath := newFileCmd.String(
		"file",
		"/dev/stdin",
		"the local path of the file to commit to the repo")
	commitMessage := newFileCmd.String(
		"msg",
		"giblab-monkey-commit",
		"the path of the file in the repo")

	newFileCmd.Parse(os.Args[2:])
	c.UpdateFile(
		projectID,
		targetBranch,
		commitMessage,
		filePath,
		localFilePath,
		silentPtr,
	)
}

func (c *Cli) runDeleteFile() {
	deleteFileCmd := flag.NewFlagSet("deletefile", flag.ExitOnError)
	silentPtr := deleteFileCmd.Bool(
		"silent", false, "shut up monkey.")
	targetBranch := deleteFileCmd.String(
		"target",
		os.Getenv("TARGET_BRANCH"),
		"the new file will be created on this git ref")
	projectID := deleteFileCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	filePath := deleteFileCmd.String(
		"path",
		"version",
		"the path of the file in the repo")
	commitMessage := deleteFileCmd.String(
		"msg",
		"giblab-monkey-commit",
		"the path of the file in the repo")

	deleteFileCmd.Parse(os.Args[2:])
	c.DeleteFile(
		projectID,
		targetBranch,
		commitMessage,
		filePath,
		silentPtr,
	)
}

func (c *Cli) runCreateMR() {
	mergeRequestCmd := flag.NewFlagSet("createmr", flag.ExitOnError)
	silentPtr := mergeRequestCmd.Bool(
		"silent", false, "shut up monkey.")
	sourceBranch := mergeRequestCmd.String(
		"source",
		os.Getenv("CI_COMMIT_REF_NAME"),
		"the source branch of the merge request")
	deleteSourceBranch := mergeRequestCmd.Bool(
		"delete-source",
		false,
		"source branch of the merge request will be removed on merge")
	targetBranch := mergeRequestCmd.String(
		"target",
		os.Getenv("TARGET_BRANCH"),
		"the target branch of the merge request")
	projectID := mergeRequestCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	assigneeID := mergeRequestCmd.String(
		"assignee",
		os.Getenv("GITLAB_USER_ID"),
		"the assignee id of the gitlab merge request")

	mergeRequestCmd.Parse(os.Args[2:])
	iid := c.MergeRequest(
		projectID,
		assigneeID,
		sourceBranch,
		targetBranch,
		deleteSourceBranch,
		silentPtr,
	)
	fmt.Printf("New MergeRequest ID: %d\n", iid)
}

func (c *Cli) runAppendDesc() {
	putMergeRequestCmd := flag.NewFlagSet("getmr", flag.ExitOnError)
	silentPtr := putMergeRequestCmd.Bool(
		"silent", false, "shut up monkey.")
	internalID := putMergeRequestCmd.String(
		"iid", "1", "the internal id of the gitlab merge request")
	projectID := putMergeRequestCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")

	putMergeRequestCmd.Parse(os.Args[2:])

	mr := c.GetMergeRequest(
		internalID,
		projectID,
		&silent,
	)

	reader := bufio.NewReader(os.Stdin)
	input, _ := ioutil.ReadAll(reader)

	mr["description"] = fmt.Sprintf(
		"%s\n%s",
		mr["description"],
		input,
	)

	c.PutMergeRequest(
		internalID,
		projectID,
		&mr,
		silentPtr,
	)

}
func (c *Cli) runGetMR() {
	getMergeRequestCmd := flag.NewFlagSet("getmr", flag.ExitOnError)
	silentPtr := getMergeRequestCmd.Bool(
		"silent", false, "shut up monkey.")
	internalID := getMergeRequestCmd.String(
		"iid", "1", "the internal id of the gitlab merge request")
	projectID := getMergeRequestCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")

	getMergeRequestCmd.Parse(os.Args[2:])

	c.GetMergeRequest(
		internalID,
		projectID,
		silentPtr)

}

func (c *Cli) runAcceptMR(iid string) {
	mergeRequestAcceptCmd := flag.NewFlagSet("acceptmr", flag.ExitOnError)
	silentPtr := mergeRequestAcceptCmd.Bool(
		"silent", false, "shut up monkey.")
	internalID := mergeRequestAcceptCmd.String(
		"iid", iid, "the internal id of the gitlab merge request")
	projectID := mergeRequestAcceptCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	sourceBranch := mergeRequestAcceptCmd.String(
		"source",
		os.Getenv("CI_COMMIT_REF_NAME"),
		"the source branch of the merge request")
	targetBranch := mergeRequestAcceptCmd.String(
		"target",
		os.Getenv("TARGET_BRANCH"),
		"the target branch of the merge request")
	deleteSourceBranch := mergeRequestAcceptCmd.Bool(
		"delete-source",
		false,
		"source branch of the merge request will be removed on merge")
	assigneeID := mergeRequestAcceptCmd.String(
		"assignee",
		os.Getenv("GITLAB_USER_ID"),
		"the assignee id of the gitlab merge request")

	mergeRequestAcceptCmd.Parse(os.Args[2:])
	c.MergeAccept(
		internalID,
		projectID,
		assigneeID,
		sourceBranch,
		targetBranch,
		silentPtr,
		deleteSourceBranch)

}

func (c *Cli) runNewTag() {
	tagCmd := flag.NewFlagSet("newtag", flag.ExitOnError)
	projectID := tagCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	tagName := tagCmd.String(
		"tag",
		"",
		"the new tag to be created")
	parentRef := tagCmd.String(
		"target",
		os.Getenv("TARGET_REF"),
		"the target source ref of the new tag")
	releaseMessage := tagCmd.String(
		"msg",
		"automated release",
		"the message attached to the new tag")
	releaseDescription := tagCmd.String(
		"desc",
		"a new release",
		"the release description for the new tag")
	silentPtr := tagCmd.Bool(
		"silent", false, "shut up monkey.")
	tagCmd.Parse(os.Args[2:])

	c.NewTag(
		projectID,
		tagName,
		parentRef,
		releaseMessage,
		releaseDescription,
		silentPtr,
	)

}

func (c *Cli) runGetTags() {
	tagCmd := flag.NewFlagSet("tags", flag.ExitOnError)
	projectID := tagCmd.String(
		"project",
		os.Getenv("CI_PROJECT_ID"),
		"the project id of the gitlab project")
	orderBy := tagCmd.String(
		"order",
		"updated",
		"tags return ordered by 'name' or 'updated'")
	sort := tagCmd.String(
		"sort",
		"desc",
		"tags ordered in 'asc' or 'desc'")
	search := tagCmd.String(
		"search",
		"",
		"tags return filtered by this expression")
	silentPtr := tagCmd.Bool(
		"silent", false, "shut up monkey.")
	tagCmd.Parse(os.Args[2:])

	c.GetTags(
		projectID,
		orderBy,
		sort,
		search,
		silentPtr,
	)

}

func main() {

	if len(os.Args) < 2 {
		fmt.Println("expected 'createmr', 'acceptmr', 'tags', 'newtag', 'newfile', 'updatefile' and 'deletefile' subcommands")
		os.Exit(1)
	}

	cli := Cli{
		client: &http.Client{
			Timeout: 15 * time.Second,
		},
		//register: &map[string]interface{}{
		//	"createmr": mergeRequestCmd,
		//	"acceptmr": mergeRequestAcceptCmd,
		//},
	}

	switch os.Args[1] {
	case "createmr":
		cli.runCreateMR()

	case "getmr":
		cli.runGetMR()

	case "append-desc":
		cli.runAppendDesc()

	case "acceptmr":
		cli.runAcceptMR("0")

	case "newtag":
		cli.runNewTag()

	case "tags":
		cli.runGetTags()

	case "newfile":
		cli.runNewFile()

	case "updatefile":
		cli.runUpdateFile()

	case "deletefile":
		cli.runDeleteFile()

	default:
		fmt.Println("expected 'createmr', 'acceptmr', 'tags', 'newtag', 'newfile', 'updatefile' and 'deletefile' subcommands")
		os.Exit(1)
	}
}
