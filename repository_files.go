package main

import (
	"bufio"
	"bytes"
	"fmt"
	"net/url"
	"os"
)

func PrepareNewFile(
	projectID,
	parentRef,
	fileContent,
	commitMessage,
	filePath *string,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		BaseAddr: os.Getenv("CI_API_V4_URL"),
		Path: fmt.Sprintf(
			"repository/files/%s", url.QueryEscape(*filePath),
		),
		ProjectID:   *projectID,
		ContentType: "application/json",
	}

	data := &MapInterface{
		"branch":         *parentRef,
		"content":        *fileContent,
		"commit_message": *commitMessage,
	}
	return request, data
}

func (c *Cli) NewFile(
	projectID,
	parentRef,
	commitMessage,
	filePath,
	localFilePath *string,
	silentPtr *bool,
) int {

	var reader *bufio.Reader
	if localFilePath == nil {
		reader = bufio.NewReader(os.Stdin)
	} else {
		f, err := os.Open(*localFilePath)
		if err != nil {
			panic(err)
		}
		reader = bufio.NewReader(f)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)
	fileContent := buf.String()

	if !*silentPtr {
		fmt.Fprintf(os.Stderr, "+++ content@%s\n%s\n",
			*filePath, fileContent,
		)
	}

	file, fileData := PrepareNewFile(
		projectID,
		parentRef,
		&fileContent,
		commitMessage,
		filePath,
	)
	fileResponse, err := c.client.Do(
		file.prepareRequest("POST", fileData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	handleResponse(silentPtr, fileResponse)
	return 0
}

func PrepareUpdateFile(
	projectID,
	parentRef,
	fileContent,
	commitMessage,
	filePath *string,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		BaseAddr: os.Getenv("CI_API_V4_URL"),
		Path: fmt.Sprintf(
			"repository/files/%s", url.QueryEscape(*filePath),
		),
		ProjectID:   *projectID,
		ContentType: "application/json",
	}

	data := &MapInterface{
		"branch":         *parentRef,
		"content":        *fileContent,
		"commit_message": *commitMessage,
	}
	return request, data
}

func (c *Cli) UpdateFile(
	projectID,
	parentRef,
	commitMessage,
	filePath *string,
	localFilePath *string,
	silentPtr *bool,
) int {
	var reader *bufio.Reader
	if localFilePath == nil {
		reader = bufio.NewReader(os.Stdin)
	} else {
		f, err := os.Open(*localFilePath)
		if err != nil {
			panic(err)
		}
		reader = bufio.NewReader(f)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)
	fileContent := buf.String()

	if !*silentPtr {
		fmt.Fprintf(os.Stderr, "+++ content@%s\n%s\n",
			*filePath, fileContent,
		)
	}

	file, fileData := PrepareUpdateFile(
		projectID,
		parentRef,
		&fileContent,
		commitMessage,
		filePath,
	)
	fileResponse, err := c.client.Do(
		file.prepareRequest("PUT", fileData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	handleResponse(silentPtr, fileResponse)
	return 0
}

func PrepareDeleteFile(
	projectID,
	parentRef,
	commitMessage,
	filePath *string,
) (*APIRequest, *MapInterface) {

	request := &APIRequest{
		BaseAddr: os.Getenv("CI_API_V4_URL"),
		Path: fmt.Sprintf(
			"repository/files/%s", url.QueryEscape(*filePath),
		),
		ProjectID:   *projectID,
		ContentType: "application/json",
	}

	data := &MapInterface{
		"branch":         *parentRef,
		"commit_message": *commitMessage,
	}
	return request, data
}

func (c *Cli) DeleteFile(
	projectID,
	parentRef,
	commitMessage,
	filePath *string,
	silentPtr *bool,
) int {

	file, fileData := PrepareDeleteFile(
		projectID,
		parentRef,
		commitMessage,
		filePath,
	)
	fileResponse, err := c.client.Do(
		file.prepareRequest("DELETE", fileData, nil, silentPtr))
	if err != nil {
		panic(err)
	}
	handleResponse(silentPtr, fileResponse)
	return 0
}
